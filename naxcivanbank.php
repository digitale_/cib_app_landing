<?php
	include 'lib/Mobile_Detect.php';
	$detect = new Mobile_Detect();
	if ($detect->isMobile() AND $detect->is('iphone'))
	{
		$location = "https://apps.apple.com/us/app/cib-az/id1541577214";
	}
	elseif($detect->isMobile() AND !$detect->is('iphone'))
	{
		$location = "https://play.google.com/store/apps/details?id=az.cib.app";
	}
	else
	{
		$location = "https://cib.az";
	}
	?>

<!doctype html>
<html lang="en">
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-THXMJ3Z');</script>
	<!-- End Google Tag Manager -->
	
	
	
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-THXMJ3Z"
	                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

</head>
<body>
<h1>Yönləndirilir...</h1>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function (){
		window.location.href = '<?=$location;?>';
    });
</script>
</body>
</html>
